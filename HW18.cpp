﻿
#include <iostream>
#include <string>

using namespace std;

class Player
{
private:
    string PlayerName;
    
public:
    int PlayerScore;
    void SetPlayerName(int PlayerID)
    {
        cout << "Set Name for player N" << PlayerID+1 << endl;
        cin >> PlayerName;
    }
    void SetPlayerScore(int PlayerID)
    {
        cout << "Set score for player N" << PlayerID+1 << endl;
        cin >> PlayerScore;
    }
    void ShowPlayerData()
    {
        cout << PlayerName << '\t' << PlayerScore<<endl;
        
    }
    void static sort(Player* l, Player* r)
    {
        int sz = r - l;
        if (sz <= 1) return;
        bool b = true;
        while (b)
        {
            b = false;
            for (Player* i = l; i + 1 < r; i++)
            {
                if (i->PlayerScore > (i + 1)->PlayerScore)
                {
                    swap(*i, *(i + 1));
                    b = true;
                }
            }
            r--;
        }
    }
};


int main()
{   
    int PlayersCout;
    cout << "How many Player you want to ADD?" << endl;
    cin >> PlayersCout;

    Player* Players = new Player[PlayersCout];
   
    
    for (int x = 0; x < PlayersCout; ++x)
    {
        Players[x].SetPlayerName(x);
        Players[x].SetPlayerScore(x);
    }
    Player::sort(Players, Players + PlayersCout);

    cout << "Name " << '\t' << "Score" << '\n';

    for (int x = 0; x < PlayersCout; ++x)
    {
        Players[x].ShowPlayerData();
    }

    
    delete[] Players;

    return 0;
}
